<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sorso_wp55');

/** MySQL database username */
define('DB_USER', 'sandra');

/** MySQL database password */
define('DB_PASSWORD', 'sorso_sandra');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'kQ6k0fw6YUxeX^dw1H9U8(0McUMPpO^OLyv4X4ZqbKy(%2u&%^*lf%7u9(N8&c7v');
define('SECURE_AUTH_KEY',  'v6^f8Ul2^Aa8i%dwc%wY5SnaKBGKC874krImpmODLQqybYsg^GzvS*xJiPsmQ*mI');
define('LOGGED_IN_KEY',    'q%ojQRy@xh*LMy4PuXmKRQcAE559yBxDqoXG)fP1JvF0MiR!wUcyCcjsFaHGSwKR');
define('NONCE_KEY',        '*%rHHVgZss4oVSs6kjCATsU)ChUpdQJDyk4CpkPgu8tiV4Ge#(kkS567@!MtdeAU');
define('AUTH_SALT',        'oKTTn6r#o#^qEWaLq)iy##aroDH7ytZgK7iYpbcku^nvK3IiahN)4XKJ!42G7EDF');
define('SECURE_AUTH_SALT', '%OCiKxCMP%77&zQghcJjPbd6c#8RT!)2U!W5t4qAsvvjptiOseTHxA2C(0(X@7Ut');
define('LOGGED_IN_SALT',   '3(5AebhXWWTnV(p4!XNud5vM#kXMo(4)PP^@NBJIS362b)vcIwEO%1ZANk2D!Kv5');
define('NONCE_SALT',       'l)CN!M!SJy0UtjUkZyUuoZbT4%1bKiD^p&IeFIa75#s#O4NS71Tk^Qdeh4E6AsgU');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define( 'WP_ALLOW_MULTISITE', true );

define ('FS_METHOD', 'direct');
