<?php
/**
 * Storefront engine room
 *
 * @package storefront
 */

/**
 * Assign the Storefront version to a var
 */
$theme              = wp_get_theme( 'storefront' );
$storefront_version = $theme['Version'];

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 980; /* pixels */
}

$storefront = (object) array(
	'version'    => $storefront_version,

	/**
	 * Initialize all the things.
	 */
	'main'       => require 'inc/class-storefront.php',
	'customizer' => require 'inc/customizer/class-storefront-customizer.php',
);

require 'inc/storefront-functions.php';
require 'inc/storefront-template-hooks.php';
require 'inc/storefront-template-functions.php';

if ( class_exists( 'Jetpack' ) ) {
	$storefront->jetpack = require 'inc/jetpack/class-storefront-jetpack.php';
}

if ( storefront_is_woocommerce_activated() ) {
	$storefront->woocommerce            = require 'inc/woocommerce/class-storefront-woocommerce.php';
	$storefront->woocommerce_customizer = require 'inc/woocommerce/class-storefront-woocommerce-customizer.php';

	require 'inc/woocommerce/class-storefront-woocommerce-adjacent-products.php';

	require 'inc/woocommerce/storefront-woocommerce-template-hooks.php';
	require 'inc/woocommerce/storefront-woocommerce-template-functions.php';
	require 'inc/woocommerce/storefront-woocommerce-functions.php';
}

if ( is_admin() ) {
	$storefront->admin = require 'inc/admin/class-storefront-admin.php';

	require 'inc/admin/class-storefront-plugin-install.php';
}

/**
 * NUX
 * Only load if wp version is 4.7.3 or above because of this issue;
 * https://core.trac.wordpress.org/ticket/39610?cversion=1&cnum_hist=2
 */
if ( version_compare( get_bloginfo( 'version' ), '4.7.3', '>=' ) && ( is_admin() || is_customize_preview() ) ) {
	require 'inc/nux/class-storefront-nux-admin.php';
	require 'inc/nux/class-storefront-nux-guided-tour.php';

	if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
		require 'inc/nux/class-storefront-nux-starter-content.php';
	}
}

/* CUSTOM CODE */
 add_action('woocommerce_cart_actions', function() {
  ?>
  <a class="button wc-backward" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>"> <?php _e( 'Continue Shopping', 'woocommerce' ) ?> </a>
  <?php
});


/*Add shipping info above the proceed to checkout button*/
add_action('woocommerce_proceed_to_checkout', 'jagels_custom_checkout_field');

function jagels_custom_checkout_field() {
    echo '<p><small>* Business day = Monday to Friday - excludes weekends and public holidays.<br/>We only deliver to:</br>

UK Mainland - mainland England, Wales, Scotland (southern & central)<br/>We do not deliver to: </br>

Highlands & Islands - AB30-38, AB44-56, FK17-99, G83, HS1-9, IV1-28, IV30-56, IV63, KA27-28, KW1-17, PA20-33, PA34-49, PA60-78, PH18-26, PH30, PH31-44, PH49-50, ZE1-3.
Northern Ireland, Isle of Man, Isles of Scilly, Channel Islands</br></small></p>';
}


function myprefix_enqueue_google_fonts() { 
	wp_enqueue_style( 'Josefin Sans', 'https://fonts.googleapis.com/css?family=Josefin+Sans:400,700' ); 
}
add_action( 'wp_enqueue_scripts', 'myprefix_enqueue_google_fonts' );



add_filter( 'woocommerce_checkout_fields', 'misha_email_first' );
 
function misha_email_first( $checkout_fields ) {
	$checkout_fields['billing']['billing_email']['priority'] = 4;
	return $checkout_fields;
}

/**
 * Note: Do not add any custom code here. Please use a custom plugin so that your customizations aren't lost during updates.
 * https://github.com/woocommerce/theme-customisations
 */
