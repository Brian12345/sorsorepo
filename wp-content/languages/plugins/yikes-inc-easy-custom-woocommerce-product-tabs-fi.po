# Translation of Stable (latest release) in Finnish
# This file is distributed under the same license as the Stable (latest release) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2016-02-19 17:03:17+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/1.0-alpha-1100\n"
"Project-Id-Version: Stable (latest release)\n"

#: yikes-inc-easy-custom-woocommerce-product-tabs.php:303
#: yikes-inc-easy-custom-woocommerce-product-tabs.php:321
msgid "HTML and text to display."
msgstr "Näytettävä teksti ja HTML."

#: yikes-inc-easy-custom-woocommerce-product-tabs.php:333
msgid "Add Another Tab"
msgstr "Lisää uusi välilehti"

#. Plugin Name of the plugin/theme
msgid "YIKES Custom Product Tabs for WooCommerce"
msgstr "YIKES Custom Product Tabs for WooCommerce"

#. #-#-#-#-#  tmp-yikes-inc-easy-custom-woocommerce-product-tabs.pot (YIKES
#. Custom Product Tabs for WooCommerce 1.4.2)  #-#-#-#-#
#. Plugin URI of the plugin/theme
#. #-#-#-#-#  tmp-yikes-inc-easy-custom-woocommerce-product-tabs.pot (YIKES
#. Custom Product Tabs for WooCommerce 1.4.2)  #-#-#-#-#
#. Author URI of the plugin/theme
msgid "http://www.yikesinc.com"
msgstr "http://www.yikesinc.com"

#. Description of the plugin/theme
msgid "Extend WooCommerce to add and manage custom product tabs. Create as many product tabs as needed per product."
msgstr "WooCommerce ohjelmaan voi lisätä uusia tuotteeseen liittyviä välilehtiä ja hallita niitä. Välelehtiä voi lisätä jokaiselle tuotteelle niin monta kuin tarvitaan."

#. Author of the plugin/theme
msgid "YIKES Inc"
msgstr "YIKES Inc"

#: yikes-inc-easy-custom-woocommerce-product-tabs.php:302
#: yikes-inc-easy-custom-woocommerce-product-tabs.php:321
msgid "Content"
msgstr "Sisältö"

#: yikes-inc-easy-custom-woocommerce-product-tabs.php:52
msgid "YIKES YIKES Custom Product Tabs for WooCommerce could not be activated because WooCommerce is not installed and active."
msgstr "YIKES Custom Product Tabs for WooCommerce -lisäosaa ei voitu ottaa käyttöön koska WooCommerce -lisäosa ei ole asennettu ja käytössä."

#: yikes-inc-easy-custom-woocommerce-product-tabs.php:53
msgid "Please install and activate "
msgstr "Asenna ja ota käyttöön, ole hyvä."

#: yikes-inc-easy-custom-woocommerce-product-tabs.php:53
msgid " before activating the plugin."
msgstr " ennen kuin otat lisäosan käyttöön."

#: yikes-inc-easy-custom-woocommerce-product-tabs.php:245
msgid "Custom Tabs"
msgstr "Mukautettu välilehdet"

#: yikes-inc-easy-custom-woocommerce-product-tabs.php:277
msgid "How To"
msgstr "Ohje"

#: yikes-inc-easy-custom-woocommerce-product-tabs.php:277
msgid "To generate tabs, click 'Add Another Tab' at the bottom of this container."
msgstr "Uusi välilehti tehdään painamalla alla olevaa 'Lisää uusi välilehti' -painiketta."

#: yikes-inc-easy-custom-woocommerce-product-tabs.php:277
msgid "To delete tabs, click 'Remove Tab' to the right of the title field."
msgstr "Välilehti poistetaan painamalla kentän oikealla puolella olevaa 'Poista välilehti' -painiketta."

#: yikes-inc-easy-custom-woocommerce-product-tabs.php:277
msgid "Note : Re-save the product to initialize the WordPress content editor on newly created tab content."
msgstr "Huomaa: Tallenna tuote uudelleen jotta WordPress muokkain käynnistyy uuden välilehden sisällölle."

#: yikes-inc-easy-custom-woocommerce-product-tabs.php:278
msgid "Help Me!"
msgstr "Ohje täällä !"

#: yikes-inc-easy-custom-woocommerce-product-tabs.php:285
#: yikes-inc-easy-custom-woocommerce-product-tabs.php:317
msgid "Remove Tab"
msgstr "Poista välilehti"

#: yikes-inc-easy-custom-woocommerce-product-tabs.php:299
#: yikes-inc-easy-custom-woocommerce-product-tabs.php:320
msgid "Tab Title"
msgstr "Välilehden otsikko"
