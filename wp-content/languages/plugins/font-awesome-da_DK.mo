��          D      l       �   �   �           +  B   D  &  �  �   �     Y     f  B                             Adds Font Awesome 5 icons to your WordPress site. Supports Font Awesome Pro. Resolves conflicts across many plugins or themes that use Font Awesome. Font Awesome https://fontawesome.com/ https://fontawesome.com/how-to-use/on-the-web/using-with/wordpress PO-Revision-Date: 2019-01-11 16:11:41+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: da_DK
Project-Id-Version: Plugins - Font Awesome - Stable (latest release)
 Tilføj Font Awesome 5 ikoner til dit WordPress sted. Understøtter Font Awesome Pro. Løser konflikter på tværs af mange plugins eller temaer, der bruger Font Awesome. Font Awesome https://fontawesome.com/ https://fontawesome.com/how-to-use/on-the-web/using-with/wordpress 