��          D      l       �   �   �           +  B   D  #  �  �   �     K     X  B   q                          Adds Font Awesome 5 icons to your WordPress site. Supports Font Awesome Pro. Resolves conflicts across many plugins or themes that use Font Awesome. Font Awesome https://fontawesome.com/ https://fontawesome.com/how-to-use/on-the-web/using-with/wordpress PO-Revision-Date: 2019-07-01 13:19:56+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: pt
Project-Id-Version: Plugins - Font Awesome - Stable (latest release)
 Adiciona ícones de Font Awesome 5 ao seu site WordPress. Suporta Font Awesome Pro. Resolve conflitos entre vários plugins ou temas que utilizem Font Awesome. Font Awesome https://fontawesome.com/ https://fontawesome.com/how-to-use/on-the-web/using-with/wordpress 